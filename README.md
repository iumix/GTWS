# GTWS

#### 介绍
一个简单的操作系统

#### 环境要求

1. NASM汇编器
2. bochs虚拟机
3. GCC编译器

#### 更新日志
### V0.5.8 --2020/3/29--
修复了head.S指令后缀的bug<br/>
修复了Makefile的兼容性问题<br/>
修复了Kernel.lds的小bug<br/>
### V0.5.7 --2020/3/27--
修复了lib.h,linkage.h,printk.h的修饰符的bug<br/>
修复了lib.h,printk.h的函数bug<br/>
修复了main.c的数值错误<br/>
修复了Makefile中head.S的编译选项错误<br/>
### V0.5.6 --2020/3/25--
新增彩色字符显示功能,
通过的新增解析显示字符printk.h,printk.c实现
### V0.5.5
新增内核通用库函数lib.h<br/>
修复了makefile的小bug
### V0.5.4
新增支持ASCII的font.h库
宏定义linkage.h库
### V0.5.3
新增屏幕显示色彩
### V0.5.2
新增链接内核脚本Kernel.lds
### V0.5.1
新增Makefile脚本，可快速编译文件
### V0.5.0
完成head.S的编写
### V0.4.2
完成了BL的编写
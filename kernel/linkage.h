/*************************
 * file name: linkage.h
 * last change: Mar 22 2020
 ************************/

#ifndef _LINKAGE_H_
#define _LINKAGE_H_

/*

*/

#define L1_CACHE_BYTES 32

#define asmlinkage __attribute__((regparm(0)))

#define ___cacheline_aligned __attribute__((__aligned__(L1_CACHE_BYTES)))

#define SYMBOL_NAME(X) X

#define SYMBOL_NAME_STR(X) #X

#define SYMBOL_NAME_LABEL(X) X##:

/*

*/

#define ENTRY(name)     \
.global SYMOL_NAME(name);   \
SYMOL_NAME_LABEL(name)

#endif

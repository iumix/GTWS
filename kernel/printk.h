/*************************
 * file name: printk.h
 * last change: Mar 23 2020
 ************************/

#ifndef __PRINTK_H__
#define __PRINTK_H__

#include <stdarg.h>
#include "font.h"
#include "linkage.h"

#define ZEROPAD 1       /* pad with zero */
#define SIGN    2       /* unsigned/signed long */
#define PLUS    4       /* show plus */
#define SPACE   8       /* space if plus */
#define LEFT    16      /* left justified */
#define SPECIAL 32      /* 0x */
#define SMALL   64      /* use 'abcdef' instead of 'ABCDEF' */

#define is_digit(c) ((c) >= '0' && (c) <= '9')

#define WHITE   0X00FFFFFF  //白
#define BLACK   0X00000000  //黑
#define RED     0X00FF0000  //红
#define ORANGE  0X00FF8000  //橙
#define YELLOW  0X00FFFF00  //黄
#define GREEN   0X0000FF00  //绿
#define BLUE    0X000000FF  //蓝
#define INDIGO  0X0000FFFF  //青
#define PURPLE  0X008000FF  //紫

/* 

*/

extern unsigned char font_ascii[256][16];

char buf[4096]={0};

struct position
{
    int XResolution;
    int YResolution;

    int XPosition;
    int YPosition;

    int XCharSize;
    int YCharSize;

    unsigned int * FB_addr;
    unsigned long FB_length;
}Pos;

/*

*/

void putchar(unsigned int * fb, int Xsize, int x, int y, unsigned int FRcolor,
            unsigned int BKcolor, unsigned char font);

/*

*/

int skip_atoi(const char **s);

/*

*/

#define do_div(n, base) ({ \
int __res;  \
__asm__("divq %%rcx":"=a" (n),"=d" (__res):"0" (n),"1" (0),"c" (base)); \
__res; \
})

/*

*/

static char * number(char * str, long num, int base, int size, int precision, int type);

/*

*/

int vsprintf(char * buf, const char *fmt, va_list args);

/*

*/

int color_printk(unsigned int FRcolor, unsigned int BKcolor, const char * fmt,...);

#endif